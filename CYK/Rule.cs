﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYK
{
    public class Rule
    {
        public string Left
        {
            get;
            set;
        }
        public string Right
        {
            get;
            set;
        }
        public Rule(string left, string right)
        {
            Left = left;
            Right = right;
        }


    }
}
