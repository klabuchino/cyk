﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CYK
{
    class Program
    {
        static string word;
        static Grammar grammar;
        static void Main()
        {
            Console.WriteLine("введите полный путь файла");
            string path = Console.ReadLine();
            string[] lines = File.ReadAllLines(@path);                                  //первая строка - слово, остальные - правила
            word = lines.First();
            string[] rules = new string[lines.Length - 1];
            Array.Copy(lines, 1, rules, 0, lines.Length - 1);                           //копируем правила в отдельный массив
            
            grammar = new Grammar(rules);

            var matrix = grammar.CYK(word);
            var isWordBelongGrammar = grammar.CheckWordInGrammar(matrix);

            if(isWordBelongGrammar)
            {
                Console.WriteLine("Cлово принадлежит грамматике");
            }
            else
            {
                Console.WriteLine("Слово не принадлежит грамматике");
            }

            grammar.OutMatrix(matrix);
            Console.ReadLine();
        }
    }
}
