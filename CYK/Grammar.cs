﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace CYK
{
    public class Grammar
    {
        public List<Rule> Rules;


        public Grammar(string[] rules)
        {
            Rules = new List<Rule>();

            for (int i = 0; i < rules.Length; i++)
            {
                string[] symbols = Regex.Split(rules[i], @"->");
                Rules.Add(new Rule(symbols[0], symbols[1]));
            }
        }

        private List<string>[,] AddLastRowMatrix(string word, int sizeWord, List<string>[,] matrix)
        {
            for (int i = 0; i < sizeWord; i++)
            {
                List<string> cellMatrix = new List<string>();
                foreach (Rule rule in Rules)
                {
                    if (rule.Right.Contains(word[i]))
                    {
                        cellMatrix.Add(rule.Left);
                    }
                }
                matrix[sizeWord - 1, i] = cellMatrix;
            }
            return matrix;
        }

        private List<string> concat(List<string> rule1, List<string> rule2)
        {
            List<string> newRules = new List<string>();
            for (int i = 0; i < rule2.Count(); i++)
            {
                for (int j = 0; j < rule1.Count(); j++)
                {
                    string str = rule1[j] + rule2[i];
                    newRules.Add(str);
                }
            }
            return newRules;

        }
        public List<string>[,] CYK(string word)
        {
            int sizeWord = word.Length;
            List<string>[,] matrix = new List<string>[sizeWord, sizeWord];

            matrix = AddLastRowMatrix(word, sizeWord, matrix);


            int qtyColumns = sizeWord - 1; //матрица треугольная => количество столбцов уменьшается

            //размер подматрицы минус 1(начинается с 0), где верхняя левая ячейка - та, в которую будем записывать результат
            //в строках ниже находятся ячейки, которые будем пересекать. при чем слева мы идем снизу вверх по вертикали, 
            //а справа идем сверху по диагонали. 
            int sizeSubmatrix = 1;

            //numLine - номер строки, в которой находится ячейка для записывания
            //перемещение снизу вверх
            for (int numLine = sizeWord - 2; numLine > -1; numLine--)
            {
                //перемещение по столбцам слева направо
                for (int numColumn = 0; numColumn < qtyColumns; numColumn++)
                {
                    List<string> cellMatrix = new List<string>();
                    List<string> newRules = new List<string>();
                    int lastLine = sizeWord - 1;

                    //первая клетка матрицы ищется подъемом снизу по вертикали до строки[sizeSubmatrix - 1]
                    //вторая клетка матрицы ищется спуском по горизонтали от ячейки[lastLine - sizeSubmatrix + 1, numColumn + 1]
                    //до ячейки[lastLine, numColumn + sizeSubmatrix]
                    for (int runner = 0; runner < sizeSubmatrix; runner++)
                    {

                        newRules.AddRange(concat(matrix[lastLine - runner, numColumn],
                            matrix[lastLine - sizeSubmatrix + 1 + runner, numColumn + 1 + runner]));
                    }

                    foreach (string newRule in newRules)
                    {
                        foreach (Rule rule in Rules)
                        {
                            if (rule.Right.Contains(newRule))
                            {
                                if (!cellMatrix.Contains(rule.Left))
                                {
                                    cellMatrix.Add(rule.Left);
                                }
                            }
                        }
                    }
                    matrix[numLine, numColumn] = cellMatrix;
                }
                qtyColumns--;
                sizeSubmatrix++;
            }

            return matrix;
        }


        public bool CheckWordInGrammar(List<string>[,] matrix)
        {
            if(matrix[0,0].Contains("S"))
            {
                return true;
            }
            return false;
        }

        public void OutMatrix(List<string>[,] matrix)
        {
            int sizeMatrix = matrix.GetLength(0);
            List<string> outCellMatrix = new List<string>();

            for (int numLine = 0; numLine < sizeMatrix; numLine++)
            {
                for(int numColumn = 0; numColumn < sizeMatrix; numColumn++)
                {                   
                    if (matrix[numLine, numColumn] != null)
                    {
                        outCellMatrix = matrix[numLine, numColumn];

                        if (outCellMatrix.Count == 0)
                        {
                            Console.Write("0 ");
                        }
                        else
                        {
                            foreach (var elem in outCellMatrix)
                            {
                                Console.Write(elem);
                            }                           
                        }
                    }
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
